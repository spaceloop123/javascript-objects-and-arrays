# JavaScript Objects and Arrays

## Tasks

#### To CSV
Return [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) representation of two-dimensional numeric array. 
For example:
```js
 [
    [  0, 1, 2, 3, 4 ],
    [ 10,11,12,13,14 ],
    [ 20,21,22,23,24 ],
    [ 30,31,32,33,34 ]
 ] 
        => 
  '0,1,2,3,4\n'
 +'10,11,12,13,14\n'
 +'20,21,22,23,24\n'
 +'30,31,32,33,34'
```
Write your code in `src/index.js` within an appropriate function `toCsvText`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Array of Squares
Transform the numeric array into the according array of squares: 

f(x) = x * x

For example:
```js
[ 0, 1, 2, 3, 4, 5 ] => [ 0, 1, 4, 9, 16, 25 ]
[ 10, 100, -1 ]      => [ 100, 10000, 1 ]
```
Write your code in `src/index.js` within an appropriate function `toArrayOfSquares`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Moving sum
Transform the numeric array to the according moving sum array:

f[n] = x[0] + x[1] + x[2] +...+ x[n] 

or
 
f[n] = f[n-1] + x[n]

For example:
```js
[ 1, 1, 1, 1, 1 ]        => [ 1, 2, 3, 4, 5 ]
[ 10, -10, 10, -10, 10 ] => [ 10, 0, 10, 0, 10 ]
[ 0, 0, 0, 0, 0]         => [ 0, 0, 0, 0, 0] 
[ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] => [ 1, 3, 6, 10, 15, 21, 28, 36, 45, 55 ]
```
Write your code in `src/index.js` within an appropriate function `getMovingSum`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Every second item
Return every second item from the specified array.
For example:
```js
[ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] => [ 2, 4, 6, 8, 10 ]
[ 'a', 'b', 'c' , null ]  => [ "b", null ]
[ "a" ] => []
```
Write your code in `src/index.js` within an appropriate function `getSecondItems`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Sequence item propagation
Propagate every item in sequence its position times: one first item, two second items, tree third items etc.
For example:
```js
[] => []
[ 1 ] => [ 1 ]
[ 'a', 'b' ] => [ 'a', 'b','b' ]
[ 'a', 'b', 'c', null ] => [ 'a', 'b','b', 'c','c','c',  null,null,null,null ]
[ 1,2,3,4,5 ] => [ 1, 2,2, 3,3,3, 4,4,4,4, 5,5,5,5,5 ]
```
Write your code in `src/index.js` within an appropriate function `propagateItemsByPositionIndex`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Sort digit names
Sort digit names.
For example:
```js
[] => []
[ 'nine','one' ]                 => [ 'one', 'nine' ]
[ 'one','two','three' ]          => [ 'one','two', 'three' ]
[ 'nine','eight','nine','eight'] => [ 'eight','eight','nine','nine']
[ 'one','one','one','zero' ]     => [ 'zero','one','one','one' ]
```
Write your code in `src/index.js` within an appropriate function `sortDigitNamesByNumericOrder`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### All occurrences
Return a number of all occurrences of the specified item in an array.
For example:
```js
[ 0, 0, 1, 1, 1, 2 ], 1 => 3
[ 1, 2, 3, 4, 5 ], 0 => 0
[ 'a','b','c','c' ], 'c'=> 2
[ null, undefined, null ], null => 2 
[ true, 0, 1, 'true' ], true => 1
```
Write your code in `src/index.js` within an appropriate function `findAllOccurences`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Distinct
Return array containing only unique values from the specified array.
For example:
```js
[ 1, 2, 3, 3, 2, 1 ] => [ 1, 2, 3 ]
[ 'a', 'a', 'a', 'a' ]  => [ 'a' ]
[ 1, 1, 2, 2, 3, 3, 4, 4] => [ 1, 2, 3, 4]
```
Write your code in `src/index.js` within an appropriate function `distinct`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Head and Tail swap
Swaps the head and tail of the specified array: 

the head (first half) of array move to the end, the tail (last half) move to the start. 
The middle element (if exists) leave on the same position.

For example:
```js
[ 1, 2, 3, 4, 5 ]   =>  [ 4, 5, 3, 1, 2 ]
 \----/   \----/         
  head     tail 
[ 1, 2 ]  => [ 2, 1 ] 
[ 1, 2, 3, 4, 5, 6, 7, 8 ]   =>  [ 5, 6, 7, 8, 1, 2, 3, 4 ]   
```
Write your code in `src/index.js` within an appropriate function `swapHeadAndTail`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-objects-and-arrays
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-objects-and-arrays/  
4. Go to folder `javascript-objects-and-arrays`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-objects-and-arrays)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
