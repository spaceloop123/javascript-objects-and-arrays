const assert = require('assert');
const tasks = require('../src');

describe('04-arrays-tasks', function() {

    it('toCsvText should convert two-dimentional numeric array to CSV format', function () {
        [
            {
                arr: [
                    [  0, 1, 2, 3, 4 ],
                    [ 10,11,12,13,14 ],
                    [ 20,21,22,23,24 ],
                    [ 30,31,32,33,34 ]
                ],
                expected:
                    '0,1,2,3,4\n'
                    +'10,11,12,13,14\n'
                    +'20,21,22,23,24\n'
                    +'30,31,32,33,34'
            }, {
            arr: [[]],
            expected: ''
        }
        ].forEach(data => {
            var actual = tasks.toCsvText(data.arr);
            assert.equal(
                data.expected,
                actual
            );
        });
    });


    it('toArrayOfSquares should convert numeric array to the array of squares', function () {
        [
            {
                arr:      [ 0, 1, 2, 3,  4,  5 ],
                expected: [ 0, 1, 4, 9, 16, 25 ]
            }, {
            arr:      [  10,   100, -1 ],
            expected: [ 100, 10000,  1 ]
        }
        ].forEach(data => {
            var actual = tasks.toArrayOfSquares(data.arr);
            assert.deepEqual(
                data.expected,
                actual
            );
        });
    });


    it('getMovingSum should convert numeric array to the according array of moving sum', function () {
        [
            {
                arr:      [ 1, 1, 1, 1, 1 ],
                expected: [ 1, 2, 3, 4, 5 ]
            }, {
            arr:      [ 10, -10, 10, -10, 10 ],
            expected: [ 10,   0, 10,   0, 10 ]
        }, {
            arr:      [ 0, 0, 0, 0, 0],
            expected: [ 0, 0, 0, 0, 0]
        }, {
            arr:      [ 1, 2, 3,  4,  5,  6,  7,  8,  9, 10 ],
            expected: [ 1, 3, 6, 10, 15, 21, 28, 36, 45, 55 ]
        }
        ].forEach(data => {
            var actual = tasks.getMovingSum(data.arr);
            assert.deepEqual(
                data.expected,
                actual
            );
        });
    });


    it('getSecondItems should return every second item from the specified array', function () {
        [
            {
                arr:      [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                expected: [    2,    4,    6,    8,    10 ]
            }, {
            arr:      [ 'a', 'b', 'c' , null ],
            expected: [      "b",       null ]
        }, {
            arr:      [ "a" ],
            expected: [     ]
        }
        ].forEach(data => {
            var actual = tasks.getSecondItems(data.arr);
            assert.deepEqual(
                data.expected,
                actual
            );
        });
    });


    it('propagateItemsByPositionIndex should propagate every item its position time', function () {
        [
            {
                arr:      [],
                expected: []
            }, {
            arr:      [ 1 ],
            expected: [ 1 ]
        }, {
            arr:      [ 'a', 'b'     ],
            expected: [ 'a', 'b','b' ]
        }, {
            arr:      [ 'a', 'b',     'c',          null                ],
            expected: [ 'a', 'b','b', 'c','c','c',  null,null,null,null ]
        }, {
            arr:      [ 1, 2,   3,     4,       5         ],
            expected: [ 1, 2,2, 3,3,3, 4,4,4,4, 5,5,5,5,5 ]
        }
        ].forEach(data => {
            var actual = tasks.propagateItemsByPositionIndex(data.arr);
            assert.deepEqual(
                data.expected,
                actual
            );
        });
    });


    it('sortDigitNamesByNumericOrder should sort digit names by its numeric value', function () {
        [
            {
                arr:      [],
                expected: []
            }, {
            arr:      [ 'nine','one' ],
            expected: [ 'one', 'nine' ]
        }, {
            arr:      [ 'one','two','three' ],
            expected: [ 'one','two', 'three' ]
        }, {
            arr:      [ 'nine','eight','nine','eight' ],
            expected: [ 'eight','eight','nine','nine' ]
        }, {
            arr:      [ 'one','one','one','zero' ],
            expected: [ 'zero','one','one','one' ]
        }, {
            arr:      [ 'nine','eight','seven','six','five','four','three','two','one','zero' ],
            expected: [ 'zero','one','two','three','four','five','six','seven','eight','nine' ]
        }
        ].forEach(data => {
            var actual = tasks.sortDigitNamesByNumericOrder(data.arr);
            assert.deepEqual(
                data.expected,
                actual
            );
        });
    });


    it('distinct should return an array of unique items from the specified array', function () {
        [
            {
                arr:      [ 1, 2, 3, 3, 2, 1 ],
                expected: [ 1, 2, 3 ]
            }, {
            arr:      [ 'a', 'a', 'a', 'a', 'a' ],
            expected: [ 'a' ]
        }, {
            arr:      [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
            expected: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
        }, {
            arr:      [ 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6 ],
            expected: [ 1, 2, 3, 4, 5, 6 ]
        }
        ].forEach(data => {
            var actual = tasks.distinct(data.arr);
            assert.deepEqual(
                data.expected,
                actual
            );
        });
    });


    it('swapHeadAndTail should swap the head and tail of the array', function () {
        [
            {
                arr:      [ 1 ],
                expected: [ 1 ]
            },{
            arr:      [ 1, 2 ],
            expected: [ 2, 1 ]
        },{
            arr:      [ 1, 2, 3 ],
            expected: [ 3, 2, 1 ]
        },{
            arr:      [ 1, 2, 3, 4 ],
            expected: [ 3, 4, 1, 2 ]
        },{
            arr:      [ 1, 2, 3, 4, 5 ],
            expected: [ 4, 5, 3, 1, 2 ]
        }
        ].forEach(data => {
            var actual = tasks.swapHeadAndTail(Array.from(data.arr));
            assert.deepEqual(
                data.expected,
                actual,
                `The result of swaping head and tail [${data.arr}] is not correct`
            );
        });
    });


    it('Functions from 04-array-test.js should not use basic loops statements', function () {
        Object.getOwnPropertyNames(tasks)
            .filter(x => tasks[x] instanceof Function)
            .forEach(f => {
                assert(
                    !/([;{]\s*(for|while)\s*\()|(\.forEach\s*\()/.test(tasks[f].toString()),
                    `Function "${f}" should not use basic loop statements (for, while or Array.forEach)! Please use specialized array methods (Array.map, Array.reduce etc).`
                );
            });
    });

});
